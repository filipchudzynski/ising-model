import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from numba import jit
from timeit import default_timer as timer
from collections import deque
import copy  

from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets

kb = 1

def generateLattice(L,random=False):
    if random:
      return np.random.choice([-1,1],(L,L))
    else:
      return np.ones((L,L))

def Magnetization(spinLattice):
    return np.mean(spinLattice.flatten())

def MagneticSusceptibility(spinLattice,L,t):
    return np.var(spinLattice.flatten())*1/t * L**2

def measureTime(func):
    def wrapper(*args,**kwargs):
      start = timer()
      ret = func(*args,**kwargs)
      end = timer()
      print(func.__name__ + " " + str(end-start))
      return ret,end-start
    return wrapper 

"""performs single simulation step
using wolff cluster algorithm
"""

@measureTime  
@jit(nopython=True)
def wolff(spins,beta,J=1):
  Lx,Ly=np.shape(spins)
  dirs = [(1,0),(-1,0),(0,1),(0,-1)]

  alreadyVisited = []
  cluster=[]
  neighborsToBeExamined=[]
  p = 1-np.e**(-2*beta*J)
  x,y = np.random.choice(Lx),np.random.choice(Ly)
      #spins[(x+d[0])%Lx,(y+d[1])%Ly]
  clusterVal = spins[x,y]#-1
  alreadyVisited.append((x,y)) #[(2,1)]
      # print("new cluster")
  cluster.append((x,y)) #[(2,1)]
  
  #initial neighbors checking
  for dir in dirs:
    if spins[(x+dir[0])%Lx, (y+dir[1])%Ly ] == clusterVal:
      if np.random.rand() < p:
        cluster.append(
          (  (x+dir[0])%Lx, (y+dir[1])%Ly )  #[(2,1),(2,2)]
        )
        neighborsToBeExamined.append( 
                                        (  (x+dir[0])%Lx, (y+dir[1])%Ly ) #[(2,2)]
                                      )
  #proceeding further down the cluster until we checked everything
  while len(neighborsToBeExamined)>0:
    (x,y) = neighborsToBeExamined.pop() #2,2   2.0
    if (x,y) not in alreadyVisited: #True   True
      alreadyVisited.append((x,y))  #[(2,1),(2,2)] #[(2,1),(2,2),(2,0)]  
      for dir in dirs:
        if ((x+dir[0])%Lx, (y+dir[1])%Ly ) not in cluster: 
          if spins[(x+dir[0])%Lx, (y+dir[1])%Ly ] == clusterVal:
            if np.random.rand() < p:
              cluster.append(
                  ((x+dir[0])%Lx, (y+dir[1])%Ly ) #[(2,1),(2,2),(2,0)]
                )
              neighborsToBeExamined.append( 
                                          (  (x+dir[0])%Lx, (y+dir[1])%Ly ) #[2,0]
                                          )
  
  # print(cluster)
  for pos in cluster:
    x,y=pos
    spins[x,y]*=-1

  return spins


def sweepNoNumba(spins,beta): 
    Lx,Ly = np.shape(spins)
    J = 1
    dir = [(1,0),(-1,0),(0,1),(0,-1)]
    
    for x in range(Lx):
        for y in range(Ly):
            sumOfSpin = 0
            for d in dir:
                sumOfSpin+=spins[(x+d[0])%Lx,(y+d[1])%Ly]
            E = 2*J*spins[x,y]*sumOfSpin
            if E <= 0:
                spins[x,y] *= -1
            elif E > 0:
                if np.random.rand() < np.exp(-beta*E):
                    spins[x,y] *= -1
    return spins

"""performs single simulation step
using metropolis algorithm
"""

@measureTime
@jit(nopython=True)
def metropolis(spins,beta): 
    Lx,Ly = np.shape(spins)
    J = 1
    dir = [(1,0),(-1,0),(0,1),(0,-1)]
    
    for x in range(Lx):
        for y in range(Ly):
            sumOfSpin = 0
            for d in dir:
                sumOfSpin+=spins[(x+d[0])%Lx,(y+d[1])%Ly]
            E = 2*J*spins[x,y]*sumOfSpin
            if E <= 0:
                spins[x,y] *= -1
            elif E > 0:
                if np.random.rand() < np.exp(-beta*E):
                    spins[x,y] *= -1
    return spins

def MonteCarloProcedure(spinLattice,beta,algorithm=wolff,Nmeasurement=0,timeConvergence=False):
    #version of algorithm used for calculations in critical state
    #it is anticipated, that in the beggining of a simulation 
    #single step of wolff algorithm will be quick, since large clusters didn't form yet
    #but as we progress the time will go up till the cluster size will be comparable with
    #system size and we can get stuck in long calculation.
    #Maximum time of single step is reached for cluster equal to system size.
    #To "thermalize" system "sufficiently" we can use following strategy


    #make a lot of steps, which are cheap
    #and as the calculations get harder reduce number of steps

    #algorithm
    #
    # repeat until Nmeasurement goes to 1 (initialy e.g = 100)
    # make a single step and measure time
    # make Nmeasurement simulation steps and measure average time
    # if time increased two times reduce n of simulation by half and repeat

    if timeConvergence == True:
      averageTime = [1,1]
      flag = 1
      while Nmeasurement>1:
        for measurement in range(Nmeasurement):
            spinLattice, elapsedTime = algorithm(spinLattice,beta)
            averageTime[1]+=elapsedTime
            yield spinLattice
        print("long waiting alg",averageTime[1]/Nmeasurement,averageTime[0]/Nmeasurement,Nmeasurement,flag)
        if flag:
          averageTime[0]=averageTime[1]
          flag=0
        if averageTime[1]/averageTime[0] > 2:
          Nmeasurement=int(Nmeasurement/2)
          flag=1
    #normal version of algorithm with fixed number of steps
    else:
      for measurement in range(Nmeasurement):
          spinLattice, _ = algorithm(spinLattice,beta)
          yield spinLattice
    return spinLattice


def magnetizationAndSusceptibilityStats(L):
    spinLattice = generateLattice(L)
    Nmeasurement = 1500
    Ntry = 10
    temps = np.linspace(1,3,20)
    resultsInTemp = []
    for t in temps:
        MagnetizationTable = []
        MagneticSusceptibilityTable = []
        for n in range(Ntry):
            #thermalization
            for i in range(Nmeasurement):
                spinLattice,_ = metropolis(spinLattice,1/t)
            results = []
            for j in range(Nmeasurement):
                spinLattice,_ = metropolis(spinLattice,1/t)
                results.append( [Magnetization(spinLattice),MagneticSusceptibility(spinLattice,L,t)])
            
            MagnetizationTable.append(np.sum(np.array(results)[:,0])/Nmeasurement)
            MagneticSusceptibilityTable.append(np.sum(np.array(results)[:,1]/Nmeasurement ))
        resultsInTemp.append([MagnetizationTable,MagneticSusceptibilityTable])
    
    Magnetizationmeans = []
    Magnetizationstd=[]
    MagneticSusceptibilitymeans = []
    MagneticSusceptibilitystd = []

    plt.imshow(spinLattice)
    plt.show()
    for i, t in enumerate(temps):
        Magnetizationmeans.append( np.mean( np.abs( resultsInTemp[i][0] ) ) )
        Magnetizationstd.append( np.std( np.abs(resultsInTemp[i][0] )) )
        MagneticSusceptibilitymeans.append( np.mean(np.var( resultsInTemp[i][1])) )
        MagneticSusceptibilitystd.append( np.std( resultsInTemp[i][1] ) )

  
    plt.errorbar(temps,Magnetizationmeans,yerr=Magnetizationstd,fmt='o')
    plt.show()
    plt.errorbar(temps,MagneticSusceptibilitymeans,yerr=MagneticSusceptibilitystd,fmt='o')
    plt.show()

    return ((temps,Magnetizationmeans,Magnetizationstd),(temps,MagneticSusceptibilitymeans,MagneticSusceptibilitystd))
    


def benchmark():
        spins=generateLattice(1000)

        _,timer = sweep(spins,3)
        print("numba",timer)

        spins=generateLattice(1000)

        start = timer()
        sweepNoNumba(spins,3)
        end = timer()
        print("no numba",end-start)


#each cluster is just a list of positions of spins contained in one cluster
def findClusters(spins,clusterVal):
  clusters = []

  #we are at position (i,i), we add it to the  v i s i t e d  p o s i t i o n s  
  # and we check if at current position is the spin of type we look for
  #if yes we add it to the c l u s t e r and check if it's neighbors are of the same type
  #if yes we add them to the c l u s t e r and add their position to stack, 
  # where we store positions we need to visit later in order to check their
  #neighbors 

  Lx,Ly = np.shape(spins)
  dirs = [(1,0),(-1,0),(0,1),(0,-1)]
  alreadyVisited = []
  for x in range(Lx):
    for y in range(Ly):
      #spins[(x+d[0])%Lx,(y+d[1])%Ly]
      if spins[x,y] == clusterVal:
        if (x,y) not in alreadyVisited:
          # print("new cluster")
          clusters.append([])
          alreadyVisited.append((x,y))
          neighborsToBeExamined=deque()
          neighborsToBeExamined.append((x,y))
          while len(neighborsToBeExamined)>0:
            # print(clusters)
            # print(neighborsToBeExamined)
            (x,y) = neighborsToBeExamined.pop()
            for dir in dirs:
              if spins[(x+dir[0])%Lx, (y+dir[1])%Ly ] == clusterVal:
                if ((x+dir[0])%Lx, (y+dir[1])%Ly) not in alreadyVisited:
                  neighborsToBeExamined.append( 
                                            (  (x+dir[0])%Lx, (y+dir[1])%Ly ) 
                                          )
           
            alreadyVisited.append((x,y))
            if (x,y) not in clusters[-1]:
              clusters[-1].append((x,y))
  clusterSizes=[]
  for cluster in clusters:
    clusterSizes.append(len(cluster))
  return clusters,clusterSizes

def get_equidistant_points_in_log_scales(start,stop,n_of_points):
        scales = 2**np.linspace(np.log2(start),np.log2(stop),n_of_points)
        scales = scales.astype(int)
        # print(scales)
        return scales

def magnify(spinLattice,plot=False,clusterType=1):
  Lx,Ly = np.shape(spinLattice)
  scales = [int(2*x) for x in np.linspace(8,int(Lx/4),20)]
  indices = list(range(Lx))
  clusterSizeDist={}
  for s in scales:
    clusterSizeDist[s]=[]
    for begx,endx in zip(indices[::s][:-1],indices[::s][1:]):
      for begy, endy in zip(indices[::s][:-1],indices[::s][1:]):
        smallLattice =  spinLattice[begx:endx,begy:endy]
        # print(smallLattice)
        if plot is True:
          fig = go.Figure(go.Heatmap(z=smallLattice))
          fig.update_xaxes(tickvals = list(range(len(list(range(begy,endy)))))[::5], ticktext=list(range(begx,endx))[::5])
          fig.update_yaxes(tickvals = list(range(len(list(range(begy,endy)))))[::5], ticktext=list(range(begy,endy))[::5])
          fig.update_layout(
            autosize=False,
            width=400,
            height=400)
          fig.show()
        clusters,clusterSizes = findClusters(smallLattice,clusterType)
        clusterSizeDist[s].append(clusterSizes)
  return clusterSizeDist


def vote(cell):
  dim=int(np.sqrt(len(cell)))
  if np.sum(cell) > 0:
    return 1
  elif np.sum(cell) == 0:
    if np.random.rand()>0.5:
      return 1
    else:
      return -1
  else:
    return -1

def renormalize(spinLattice,s=2):
  Lx,Ly = np.shape(spinLattice)
  # scales = [int(2*x) for x in np.linspace(8,int(Lx/4),20)]
  indices = list(range(Lx+s))
  # clusterSizeDist={}
  cellLattice=[]
  # clusterSizeDist[s]=[]
    
  for begy,endy in zip(indices[::s][:-1],indices[::s][1:]):
    cellLattice.append([])
    for begx, endx in zip(indices[::s][:-1],indices[::s][1:]):
      cellLattice[-1].append(spinLattice[begy:endy,begx:endx].flatten())
  
  coarseGrainedLattice = None
  for row in cellLattice:
    coarseGrainedRow=None
    for cell in row:
      if coarseGrainedRow is None:
        coarseGrainedRow = [vote(cell)]
      else:
        coarseGrainedRow.append(vote(cell))
    coarseGrainedRow=np.array([coarseGrainedRow])
    if coarseGrainedLattice is None:
      coarseGrainedLattice = coarseGrainedRow
    else:
      coarseGrainedLattice=np.concatenate((coarseGrainedLattice,coarseGrainedRow),axis=0)

  return coarseGrainedLattice

def invariance_check(L,Tfactor,Nmeasurement=0,zoom=4,spinLattice=None,timeConvergence=False):
  # L=1600
  if spinLattice is None:
    spinLattice = generateLattice(L,True)
  Tc =2.269158
  T=Tc*Tfactor
  beta = 1/T
  coarse=[]
  for results,spins in MonteCarloProcedureNumba(spinLattice,beta,algorithm=wolff,
                                                Nmeasurement=Nmeasurement,
                                                timeConvergence=timeConvergence):
    pass
  
  coarse.append(spins)

  for ind in range(zoom):
    coarse.append(renormalize(coarse[-1],s=2))

  return coarse

def invariance_histogram(coarseAssembly):
  #fig_ren = go.Figure()
  clusterSizes={2**ind:[] for ind in range(len(coarseAssembly[0]))}
  for coarse in coarseAssembly:
      for ind,lattice in enumerate(coarse):
        zoomSquareInd=int(np.random.rand()*np.shape(lattice)[0]/100)+1
        _,clusterSizesPos = findClusters(lattice[(zoomSquareInd-1)*100:zoomSquareInd*100,
                                                 (zoomSquareInd-1)*100:zoomSquareInd*100],1)
        _,clusterSizesNeg = findClusters(lattice[(zoomSquareInd-1)*100:zoomSquareInd*100,
                                                 (zoomSquareInd-1)*100:zoomSquareInd*100],-1)
        clusterSizes[2**ind]+=clusterSizesPos+clusterSizesNeg

  for key in clusterSizes.keys():
      hist,bins = np.histogram(clusterSizes[key],bins=max(clusterSizes[key]))

  return clusterSizes

@jit(nopython=True)
def correlationsMagnetization(results,average,maxLag):
  correlationTime = np.zeros(maxLag)
  for lag in range(maxLag):
    for ind,result in enumerate(results[:-lag]):
        correlationTime[lag] += (results[ind]-average) * (results[ind+lag]-average)
  for ind,result in enumerate(results):
        correlationTime[0] += (results[ind]-average) * (results[ind]-average)
  return correlationTime

@jit(nopython=True)
def correlationsSingleSpins(spins,results,maxLag,L):
  correlationTime = np.zeros(maxLag)
  for lag in range(maxLag):
    for ind,result in enumerate(results[:-lag]):
      for posx in range(L):
        for posy in range(L):
          correlationTime[lag] += (spins[ind][posx,posy]-result) * (spins[ind+lag][posx,posy]-results[ind+lag])


  return correlationTime
